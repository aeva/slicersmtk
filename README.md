SlicerSMTK: An extension module for aevaSlicer
---------------

SlicerSMTK is an extension for [aevaSlicer](http://gitlab.kitware.com/aeva/aevaSlicer)
(which is based on [3D Slicer](https://github.com/Slicer/Slicer)) which adds the functionality
for exporting Slicer's MRML tree as an smtk output file, which can be further read by
[aevaCMB](https://gitlab.kitware.com/aeva/aeva) application.

<p align="center">
  <img src="LogoFull.png" alt="aeva Logo"/>
</p>

## Overview

The SlicerSMTK extension allows aevaSlicer to export all the loaded datasets such as
images, segmentations, surface and solid meshes as a single `.aeva.smtk` resource file.
This file uses the `json` format which can then be loaded into the aevaCMB application.
This allows for a faster / more efficient data transfer between aevaSlicer and aevaCMB.

## License

This software is licensed under the terms of the [Apache Licence Version 2.0](LICENSE).
