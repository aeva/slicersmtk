
set(proj Boost)

# Set dependency list
set(${proj}_DEPENDS
  ""
  )

# By default, expect Boost to be installed on the system
if(NOT DEFINED Boost_DIR)
  set(${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj} ON)
endif()

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj)

#-----------------------------------------------------------------------------
# Adapted from SMTK/CMakeLists.txt

# List of Boost features used:
# * Atomic (dependency of Filesystem)
# * Date Time
# * Filesystem
# * String algorithms
# * UUID Generation
set(required_boost_components
  atomic date_time filesystem system)

# Some c++ compilers do not support regex, so we may need Boost's regex library.
if(CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR
    (CMAKE_CXX_COMPILER_ID MATCHES "GNU" AND
      (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.9.0")) OR
    (WIN32 AND MSVC))
#we definitely do not need regex support
elseif (CMAKE_CXX_COMPILER_ID MATCHES "GNU" AND
    CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.9.0")
#we definitely need regex support
  list(APPEND required_boost_components regex)
else()
#we may need regex support, but rather than try-compile let's just use boost
  list(APPEND required_boost_components regex)
endif()
#-----------------------------------------------------------------------------

if(${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
  unset(Boost_DIR CACHE)
  #-----------------------------------------------------------------------------
  # Copied from SMTK/CMakeLists.txt

  set(SMTK_MINIMUM_BOOST_VERSION 1.64.0)
  find_package(Boost ${SMTK_MINIMUM_BOOST_VERSION}
               COMPONENTS ${required_boost_components} REQUIRED)
  #-----------------------------------------------------------------------------
endif()

# Sanity checks
if(DEFINED Boost_DIR AND NOT EXISTS ${Boost_DIR})
  message(FATAL_ERROR "Boost_DIR variable is defined but corresponds to nonexistent directory")
endif()

if(NOT DEFINED ${proj}_DIR AND NOT ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
  message(FATAL_ERROR "Boost is expected to be either specified using Boost_DIR or installed on the system")
else()
  ExternalProject_Add_Empty(${proj} DEPENDS ${${proj}_DEPENDS})
endif()

mark_as_superbuild(Boost_DIR:PATH)
ExternalProject_Message(${proj} "Boost_DIR:${Boost_DIR}")

set(SMTK_BOOST_REQUIRED_COMPONENTS ${required_boost_components})
mark_as_superbuild(SMTK_BOOST_REQUIRED_COMPONENTS:STRING)
ExternalProject_Message(${proj} "SMTK_BOOST_REQUIRED_COMPONENTS:${SMTK_BOOST_REQUIRED_COMPONENTS}")
