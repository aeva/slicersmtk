//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

/*==============================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==============================================================================*/

/// Qt includes
#include <QFileInfo>

// CTK includes
#include <ctkFlowLayout.h>
#include <ctkUtils.h>

/// SMTKFileReader includes
#include "qSlicerIOOptions_p.h"
#include "qSlicerSMTKFileReaderOptionsWidget.h"
#include "ui_qSlicerSMTKFileReaderOptionsWidget.h"

//-----------------------------------------------------------------------------
/// \ingroup SlicerRt_QtModules_SMTKFileReader
class qSlicerSMTKFileReaderOptionsWidgetPrivate
  : public qSlicerIOOptionsPrivate
  , public Ui_qSlicerSMTKFileReaderOptionsWidget
{
public:
};

//-----------------------------------------------------------------------------
qSlicerSMTKFileReaderOptionsWidget::qSlicerSMTKFileReaderOptionsWidget(QWidget* parentWidget)
  : qSlicerIOOptionsWidget(new qSlicerSMTKFileReaderOptionsWidgetPrivate, parentWidget)
{
  Q_D(qSlicerSMTKFileReaderOptionsWidget);
  d->setupUi(this);

  ctkFlowLayout::replaceLayout(this);

  connect(d->UseImageIntensityScaleAndOffsetCheckBox, SIGNAL(toggled(bool)),
          this, SLOT(updateProperties()));

  // Image intensity scale and offset turned off by default
  d->UseImageIntensityScaleAndOffsetCheckBox->setChecked(false);
}

//-----------------------------------------------------------------------------
qSlicerSMTKFileReaderOptionsWidget::~qSlicerSMTKFileReaderOptionsWidget() = default;

//-----------------------------------------------------------------------------
void qSlicerSMTKFileReaderOptionsWidget::updateProperties()
{
  Q_D(qSlicerSMTKFileReaderOptionsWidget);

  d->Properties["imageIntensityScaleAndOffset"] = d->UseImageIntensityScaleAndOffsetCheckBox->isChecked();
}
