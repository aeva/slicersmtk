//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Harald Scheirich, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

// Slicer includes
#include <ctkCoreTestingMacros.h>
#include <vtkMRMLScene.h>

// SlicerSMTK includes
#include <vtkSlicerSMTKFileReaderLogic.h>

// Std includes
#include <iostream>


int vtkSlicerSMTKFileReaderLogicImportEmptySceneTest(int argc, char* argv[])
{
  bool test_success = true;
  std::string fileName =  "EmptyScene.aeva.smtk";
  std::string scratchDir = ".";
  // Default dataDir path for debugging the test.
  // Actual test-run will set this path through command line argument.
  // Check the CMakeLists file to see what args are passed.
  std::string dataDir = "../../../../../../../SlicerSMTK/Data/Testing/";
  if (argc >= 3)
  {
    scratchDir = std::string(argv[2]);
  }

  vtkNew<vtkMRMLScene> scene;
  vtkNew<vtkSlicerSMTKFileReaderLogic> logic;

  try
  {
    CHECK_BOOL(logic->ImportMRMLSceneFromAevaSMTKResources(scene, dataDir + fileName, dataDir).empty(), true);
    CHECK_INT(scene->GetNumberOfNodes(), 0);
  } 
  catch (std::exception& e)
  {
    std::cerr << "Unexpected Exception occurred: " << e.what() << endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
