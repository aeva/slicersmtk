//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

/*==============================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==============================================================================*/

#ifndef __qSlicerSMTKFileReaderPluginWidget_h
#define __qSlicerSMTKFileReaderPluginWidget_h

// SlicerQt includes
#include "qSlicerAbstractModuleWidget.h"

// SMTKFileReader includes
#include "qSlicerSMTKFileReaderModuleExport.h"

class qSlicerSMTKFileReaderPluginWidgetPrivate;

/// \ingroup SlicerRt_QtModules_SMTKFileReader
class Q_SLICER_SMTKFILEREADER_EXPORT qSlicerSMTKFileReaderPluginWidget :
  public qSlicerAbstractModuleWidget
{
  Q_OBJECT
public:
  typedef qSlicerAbstractModuleWidget Superclass;
  qSlicerSMTKFileReaderPluginWidget(QWidget *parent=nullptr);
  ~qSlicerSMTKFileReaderPluginWidget() override;

protected:
  QScopedPointer<qSlicerSMTKFileReaderPluginWidgetPrivate> d_ptr;
  void setup() override;

private:
  Q_DECLARE_PRIVATE(qSlicerSMTKFileReaderPluginWidget);
  Q_DISABLE_COPY(qSlicerSMTKFileReaderPluginWidget);
};

#endif
