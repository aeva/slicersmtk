//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

/*==============================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==============================================================================*/

#ifndef __qSlicerSMTKFileWriterPlugin
#define __qSlicerSMTKFileWriterPlugin

// SlicerQt includes
#include "qSlicerFileWriter.h"

class qSlicerSMTKFileWriterPluginPrivate;
class vtkSlicerSMTKFileWriterLogic;

//-----------------------------------------------------------------------------
/// \ingroup SlicerRt_QtModules_SMTKFileWriter
class qSlicerSMTKFileWriterPlugin
  : public qSlicerFileWriter
{
  Q_OBJECT

public:
  typedef qSlicerFileWriter Superclass;
  qSlicerSMTKFileWriterPlugin(QObject* parent = nullptr);
  qSlicerSMTKFileWriterPlugin(vtkSlicerSMTKFileWriterLogic* logic, QObject* parent = nullptr);
  ~qSlicerSMTKFileWriterPlugin() override;

  QString description()const override;
  IOFileType fileType()const override;

  /// Return true if the object is handled by the writer.
  bool canWriteObject(vtkObject* object)const override;

  QStringList extensions(vtkObject* object)const override;
  // qSlicerIOOptions* options()const override;

  /// Write the node identified by nodeID into the fileName file.
  /// Returns true on success.
  bool write(const qSlicerIO::IOProperties& properties) override;

  // Extra functions
  vtkSlicerSMTKFileWriterLogic* logic()const;
  void setLogic(vtkSlicerSMTKFileWriterLogic* logic);

protected:
  // Write to smtk (json) format.
  bool writeToAevaSMTK(const qSlicerIO::IOProperties& properties);

  QScopedPointer<qSlicerSMTKFileWriterPluginPrivate> d_ptr;

private:
  Q_DECLARE_PRIVATE(qSlicerSMTKFileWriterPlugin);
  Q_DISABLE_COPY(qSlicerSMTKFileWriterPlugin);
};

#endif
