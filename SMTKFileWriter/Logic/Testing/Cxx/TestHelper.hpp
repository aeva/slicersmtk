//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

#ifndef __TestHelper_hpp
#define __TestHelper_hpp

// Slicer includes
#include <vtkMRMLScene.h>

// SlicerSMTK includes
#include <vtkSlicerSMTKFileWriterLogic.h>
#include "Testing/Cxx/SlicerSMTKCommonTestHelper.hpp"

// smtk includes
#include <smtk/attribute/Attribute.h>
#include <smtk/attribute/IntItem.h>
#include <smtk/attribute/FileItem.h>
#include <smtk/attribute/ResourceItem.h>
#include <smtk/common/testing/cxx/helpers.h>
#include <smtk/operation/Manager.h>
#include <smtk/operation/Operation.h>

// aeva-session includes
#include <smtk/session/aeva/operators/Read.h>
#include <smtk/session/aeva/Registrar.h>
#include <smtk/session/aeva/Resource.h>

using smtk::session::aeva::Read;

namespace
{

smtk::session::aeva::Resource::Ptr testExportSceneToAevaSMTKFile(
    bool& test_success,
    vtkMRMLScene* scene,
    const std::string& outputFilename,
    const std::string& scratchDir)
{
  // Write out the generated scene.
  vtkNew<vtkSlicerSMTKFileWriterLogic> logic;
  test_success &= (bool)test(
    logic->ExportMRMLSceneToAevaSMTKResources(scene, outputFilename, scratchDir),
    "Export empty scene failed.");

  return readAevaSMTKFile(test_success, outputFilename, scratchDir);
}

} // end of anonymous namespace

#endif

