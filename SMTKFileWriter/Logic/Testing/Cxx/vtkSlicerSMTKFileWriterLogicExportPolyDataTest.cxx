//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

// VTK  includes
#include <vtkErrorCode.h>
#include <vtkSTLReader.h>

// Slicer includes
#include <vtkMRMLModelNode.h>
#include <vtkMRMLScene.h>

// smtk includes
#include <smtk/model/Face.h>

// SlicerSMTK includes
#include "TestHelper.hpp"

int vtkSlicerSMTKFileWriterLogicExportPolyDataTest(int argc, char* argv[])
{
  const std::string outputFilename = "PolyScene.aeva.smtk";
  const std::string inputFilename = "/stl/box.stl";

  bool test_success = true;
  std::string scratchDir = ".";

  // Default dataDir path for debugging the test.
  // Actual test-run will set this path through command line argument.
  // Check the CMakeLists file to see what args are passed.
  std::string dataDir = "../../../../../../../aeva-session/data";
  if (argc >= 3)
  {
    dataDir = std::string(argv[1]);
    scratchDir = std::string(argv[2]);
  }

  // PREPARE INPUT:
  vtkNew<vtkSTLReader> stlReader;
  stlReader->SetFileName(std::string(dataDir + inputFilename).c_str());
  stlReader->Update();
  auto boxPolyData = stlReader->GetOutput();
  test_success &= (bool)test(stlReader->GetErrorCode() == vtkErrorCode::NoError,
      "Reading input box polydata failed.");
  test_success &= (bool)test(!!boxPolyData, "Reading input box polydata failed.");

  vtkNew<vtkMRMLModelNode> polyNode;
  polyNode->SetAndObservePolyData(boxPolyData);
  vtkNew<vtkMRMLScene> scene;
  scene->AddNode(polyNode);

  // RUN LOGIC AND READ OUTPUT:
  auto readResource = testExportSceneToAevaSMTKFile(test_success, scene, outputFilename, scratchDir);

  // Access all of the model's faces
  smtk::model::EntityRefs faces =
      readResource->entitiesMatchingFlagsAs<smtk::model::EntityRefs>(smtk::model::FACE);

  test_success &= (bool)test(!faces.empty(), "No faces");

  smtk::model::Face face = *faces.begin();

  auto readSession = readResource->session();
  vtkSmartPointer<vtkDataSet> facePD =
      vtkDataSet::SafeDownCast(readSession->findStorage(face.entity()));

  test_success &= (bool)test(!!facePD.Get(), "No geometry for face");

  std::ostringstream explanation;
  explanation << "Face geometry contains an unexpected number of points and/or cells\n"
      << facePD->GetNumberOfPoints() << " " << facePD->GetNumberOfCells();

  test_success &= (bool)test(facePD->GetNumberOfPoints() == 16 && facePD->GetNumberOfCells() == 24,
      explanation.str());

  // report the overall success of this test:
  if (test_success)
  {
      return EXIT_SUCCESS;
  }
  else
    return EXIT_FAILURE;
}
