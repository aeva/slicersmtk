//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

// Slicer includes
#include <vtkMRMLScene.h>

// SlicerSMTK includes
#include "TestHelper.hpp"

int vtkSlicerSMTKFileWriterLogicExportEmptySceneTest(int argc, char* argv[])
{
  bool test_success = true;
  const std::string outputFilename = "EmptyScene.aeva.smtk";
  std::string scratchDir = ".";
  if (argc >= 3)
  {
    scratchDir = std::string(argv[2]);
  }

  vtkNew<vtkMRMLScene> scene;
  vtkNew<vtkSlicerSMTKFileWriterLogic> logic;

  // Write out an empty scene
  test_success &= (bool)test(logic->ExportMRMLSceneToAevaSMTKResources(scene, outputFilename, scratchDir), "Export empty scene failed.");

  // RUN LOGIC AND READ OUTPUT:
  testExportSceneToAevaSMTKFile(test_success, scene, outputFilename, scratchDir);

  // report the overall success of this test:
  if(test_success)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}
